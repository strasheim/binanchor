### Execution on Login

Making sure your binary is called as part of the login process.


This needs to be added to /etc/pam.d/sshd 
```
# Login Email Notification
session required pam_exec.so /usr/sbin/binanchor
```

It printing information to syslog. It will show who you are and which groups you belong to. 
The program runs as root, yet has the user info as ENVs available. It's part of PAM and can be trusted.

The permissions need to be 0700 otherwise it will abort running.

### Trust your session

- walk up 2 level in the PID information chain to see if you it's a grand child of SSHd.
- check if that is SSHd
- check your own permissions and location on the disk 

### This does not much by itself

It will only print username and group memberships to syslog. 

