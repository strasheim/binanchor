package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"
	"os"
	"os/user"
	"strconv"
	"strings"
	"syscall"
)

func main() {
	logwriter, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err == nil {
		log.SetFlags(0)
		log.SetOutput(logwriter)
	}

	if hardened() {
		username, principles := particulars()
		log.Println(username.Name, principles)
	}
}

func particulars() (myself *user.User, groupList string) {

	log.Println("# Can we do something for you", os.Getenv("PAM_USER"), "on", os.Getenv("PAM_TYPE"), "?")

	myself, err := user.Lookup(os.Getenv("PAM_USER"))
	if err != nil {
		log.Println("Can't lookup the user", err)
	}
	groups, err := myself.GroupIds()
	if err != nil {
		log.Println(err)
	}
	var myGroups []string
	for _, v := range groups {
		mygroup, err := user.LookupGroupId(v)
		if err != nil {
			log.Println(err)
		}
		myGroups = append(myGroups, mygroup.Name)
	}
	groupList = strings.Join(myGroups, ",")
	return

}

func hardened() bool {

	// Checking of Proc is indeed Proc

	buf := &syscall.Statfs_t{}
	err := syscall.Statfs("/proc", buf)
	if err != nil {
		log.Println("/proc :", err)
		return false
	}
	if buf.Type != 40864 {
		log.Println("The proc filesystem appears to be fake")
		return false
	}

	// Making sure file permissions are inline
	call, err := ioutil.ReadFile("/proc/" + strconv.Itoa(os.Getpid()) + "/cmdline")
	if strings.HasPrefix(string(call), "/") {
		call = call[:len(call)-1]
		info, err := os.Stat(string(call))
		if err != nil {
			log.Println("Stats failed:", err)
			return false
		}
		filemod := info.Mode()
		if !filemod.IsRegular() {
			log.Println("executable does not appear to be a regular file")
			return false
		}
		if filemod.String() != "-rwx------" {
			log.Println("File permissions are too open, please change to 0700", filemod.String())
			return false
		}

	} else {
		log.Println(os.Args[0], "has to called with the full path")
		return false
	}

	// Checking If ParentParent is SSHD
	dat, err := ioutil.ReadFile("/proc/" + strconv.Itoa(os.Getppid()) + "/status")
	if err != nil {
		log.Println(err)
		return false
	}
	pppid := strings.Split(string(dat), "\n")
	ParentParent := strings.Replace(pppid[6], "PPid:\t", "", -1)
	ppcommand, err := ioutil.ReadFile("/proc/" + ParentParent + "/cmdline")
	if err != nil {
		log.Println(err)
		return false
	}
	// Strng can have and does have 0 bytes with it. Building it as bytes array
	sshd := []byte{47, 117, 115, 114, 47, 115, 98, 105, 110, 47, 115, 115, 104, 100, 0, 45, 68, 0}
	if string(ppcommand) != string(sshd) {
		log.Println("SSHD isn't named called correctly", string(ppcommand))
		return false
	}

	// Checking if sshd is bound to port 22 on 0.0.0.0
	tcp, err := ioutil.ReadFile("/proc/" + ParentParent + "/net/tcp")
	if err != nil {
		fmt.Println(err)
	}
	listening := false
	lines := strings.Split(string(tcp), "\n")
	for _, v := range lines {
		if strings.Contains(string(v), "00000000:0016 00000000:0000") {
			listening = true
		}
	}
	if !listening {
		log.Println("Couldn't find parent SSHD bound to 0.0.0.0:22")
		return false
	}

	// Checking of SSHD is run by root
	uidblog, err := ioutil.ReadFile("/proc/" + ParentParent + "/status")
	if err != nil {
		log.Println(err)
	}
	lines = strings.Split(string(uidblog), "\n")
	short := strings.Replace(lines[8], "\t", "", -1)

	if short != "Uid:0000" {
		log.Println("SSHD isn't run by root")
		return false
	}

	// If we made it till here all checks have been okay
	return true
}
